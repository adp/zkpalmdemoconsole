﻿/*

SDK искать папке с SDK. Все константы и переменные описаны в проекте на C++
Инициализированные доп методы в папке с задачей в файле Program

Метод ZKPalm_GetLastLiveValue - распознование ладони
Ширина и высота изображения подгружаются автоматически
 
Сейчас сделан метод Верификации до распознования отпечатка. Также должен быть метод регистрации отпечатка в БД
 */

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;

namespace ZKPalmDemoConsole
{
    internal static class Program
    {
        private const int MAX_TEMPLATE_SIZE = 2048;
        private const int ENROLL_CNT = 3;
        private const int ZKPALM_ERR_OK = 0;
        private const int ZKPALM_ERR_NOT_DETECTED = -8;
        private const int ZKPALM_ERR_EXTRACT_FAIL = -13;

        private static int m_nWidth;
        private static int m_nHeight;
        private static IntPtr m_hDevice;
        private static IntPtr m_hDBHandle;
        private static byte[] m_pImgBuffer;
        private static bool saveTemplate;

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_Init();

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetVersion( char[] version, int len );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_Terminate();

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetDeviceCount();

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern IntPtr ZKPalm_OpenDevice( int index );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_CloseDevice( IntPtr handle );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_SetParameter( IntPtr handle, int paramCode, ref int paramValue, int size );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetParameter( IntPtr handle, int paramCode, ref int paramValue, ref int size );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_CapturePalmImageAndTemplate( IntPtr handle, byte[] imgBuffer, int cbImgBuffer, char[] pTemplate, ref int cbTemplate, ref int quality, int[] pCheckAreaRect, IntPtr resverd );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_Verify( IntPtr handle, char[] verTemplate1, int cbVerTemplate1, char[] verTemplate2, int cbVerTemplate2 );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern IntPtr ZKPalm_DBInit( IntPtr handle );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBFree( IntPtr dbHandle );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBAdd( IntPtr dbHandle, char[] id, IntPtr[] pRegTemplates, int count );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBAddEx( IntPtr dbHandle, char[] id, char[] pRegTemplates1, char[] pRegTemplates2, char[] pRegTemplates3, int count );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBDel( IntPtr dbHandle, char[] id );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBClear( IntPtr dbHanle );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBCount( IntPtr dbHandle );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_DBIdentify( IntPtr dbHandle, char[] verTemplate, int cbVerTemplate, char[] id, ref int score, int minScore, int maxScore );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_SetParameterEx( IntPtr handle, int paramCode, int paramValue );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetParameterEx( IntPtr handle, int paramCode, ref int paramValue );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_ExtractTempFromFile( IntPtr handle, char[] bitmapfile, char[] temp, int[] cbTemplate );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetHardwareId( char[] hwId, int[] length );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_SetLicData( char[] licdata, int licLen );

        [DllImport( "ZKPalmAPI.dll" )]
        private static extern int ZKPalm_GetLastLiveValue( IntPtr handle, ref int liveValue );

        private static void Main()
        {
            Init();
            Close();
        }

        private static void Init()
        {
            var ret = ZKPalm_Init();

            if ( ZKPALM_ERR_OK == ret )
                Console.WriteLine( "\nINIT TRUE\n" );

            m_hDevice = ZKPalm_OpenDevice( 0 );

            if ( m_hDevice == IntPtr.Zero )
                Console.WriteLine( "\nOPEN FAILED\n" );

            m_hDBHandle = ZKPalm_DBInit( m_hDevice );

            if ( m_hDBHandle == IntPtr.Zero )
                Console.WriteLine( "\nDBInit FAILED\n" );

            var paramSize = sizeof( int );
            ZKPalm_GetParameter( m_hDevice, 1, ref m_nWidth, ref paramSize );

            paramSize = sizeof( int );
            ZKPalm_GetParameter( m_hDevice, 2, ref m_nHeight, ref paramSize );

            m_pImgBuffer = new byte[ m_nWidth * m_nHeight ];
            var m_preRegTemplates = new char[ ENROLL_CNT ][];

            for ( var i = 0; i < ENROLL_CNT; i++ )
            {
                m_preRegTemplates[ i ] = new char[ MAX_TEMPLATE_SIZE ];
            }

            do
            {
                GetTemplate();
                Thread.Sleep( 200 );
            } while ( !saveTemplate );
        }

        private static void GetTemplate()
        {
            var szTemplate = new char[ MAX_TEMPLATE_SIZE ];
            var nTemplateSize = MAX_TEMPLATE_SIZE;
            var nQuality = 0;
            var ret = ZKPalm_CapturePalmImageAndTemplate( m_hDevice, m_pImgBuffer, m_nWidth * m_nHeight, szTemplate, ref nTemplateSize, ref nQuality, null, IntPtr.Zero );
            if ( ZKPALM_ERR_OK == ret )
            {
                var nPalmlive = 0;
                ZKPalm_GetLastLiveValue( m_hDevice, ref nPalmlive );

                SaveBitmap( m_nWidth, m_nHeight, m_pImgBuffer );

                saveTemplate = true;
            }

            else if ( ZKPALM_ERR_NOT_DETECTED == ret || ZKPALM_ERR_EXTRACT_FAIL == ret )
            {
                Console.WriteLine();
                Console.WriteLine( "Please put your palm on the sensor!" );
                Console.WriteLine();
            }
        }

        private static void SaveBitmap( int width, int height, byte[] imageData )
        {
            var data = new byte[ width * height * 4 ];

            var pixel = 0;

            for ( var i = 0; i < width * height; i++ )
            {
                var value = imageData[ i ];

                data[ pixel++ ] = value;
                data[ pixel++ ] = value;
                data[ pixel++ ] = value;
                data[ pixel++ ] = 0;
            }

            var unmanagedPointer = Marshal.AllocHGlobal( data.Length );
            Marshal.Copy( data, 0, unmanagedPointer, data.Length );

            using ( var image = new Bitmap( width, height, width * 4, PixelFormat.Format32bppRgb, unmanagedPointer ) )
                image.Save( "D:\\imageFromSharp.bmp" );

            Marshal.FreeHGlobal( unmanagedPointer );
        }

        private static void Close()
        {
            ZKPalm_DBFree( m_hDBHandle );
            ZKPalm_CloseDevice( m_hDevice );
        }
    }
}